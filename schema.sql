USE DBConferences;

CREATE USER 'myUser'@'%' IDENTIFIED BY 'myUser';

GRANT SELECT, INSERT, UPDATE, DELETE ON `DBConferences`.* TO 'myUser'@'%' IDENTIFIED BY 'myUser';

GRANT ALL PRIVILEGES ON `DBConferences`.* TO 'admin'@'%' IDENTIFIED BY 'admin' WITH GRANT OPTION;

CREATE TABLE Technicien (
    Email Varchar (50) NOT NULL,
    Nom Varchar (50) NOT NULL,
    Prenom Varchar (50) NOT NULL,
    Password Varchar (50) NOT NULL,

    CONSTRAINT PK_Technicien PRIMARY KEY (Email)
);

CREATE TABLE Medecin (
    Email Varchar (50) NOT NULL,
    Nom Varchar (50) NOT NULL,
    Prenom Varchar (50) NOT NULL,
    Password Varchar (50) NOT NULL,

    CONSTRAINT PK_Medecin PRIMARY KEY (Email)
);

CREATE TABLE Commercial (
    Email Varchar (50) NOT NULL,
    Nom Varchar (50) NOT NULL,
    Prenom Varchar (50) NOT NULL,
    Password Varchar (50) NOT NULL,

    CONSTRAINT PK_Commercial PRIMARY KEY (Email)
);

CREATE TABLE Conference (
    PK_Conference Int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Titre Varchar (50) NOT NULL,
    Lieu JSON NULL,
    DateDebut Date NOT NULL,
    DateFin Date NOT NULL,
    -- C1
    NbMaxMedecin Int NOT NULL DEFAULT 5,
    TechnicienValide Varchar (50),
    TechnicienResp Varchar(50),
    CommercialResp Varchar(50),

    CONSTRAINT FK_TechnicienValide FOREIGN KEY (TechnicienValide) REFERENCES Technicien(Email),
    CONSTRAINT FK_TechnicienResp FOREIGN KEY (TechnicienResp) REFERENCES Technicien(Email),
    CONSTRAINT FK_CommercialResp FOREIGN KEY (CommercialResp) REFERENCES Commercial(Email),

    -- C6 / Cr4
    CONSTRAINT check_dates CHECK (DateDebut < DateFin)
);


CREATE TABLE Invitation (
    Medecin Varchar (50),
    Conference Int,
    Commercial Varchar (50) NOT NULL,

    CONSTRAINT PK_Invitation PRIMARY KEY (Medecin,Conference),
    CONSTRAINT FK_Commercial_Invitation FOREIGN KEY (Commercial) REFERENCES Commercial(Email),
    CONSTRAINT FK_Medecin_Invitation FOREIGN KEY (Medecin) REFERENCES Medecin(Email),
    CONSTRAINT FK_Conference_Invitation FOREIGN KEY (Conference) REFERENCES Conference(PK_Conference)
);

CREATE TABLE Inscription (
    Medecin Varchar (50),
    Conference Int,
    DateInscription Date NOT NULL,
    Commercial Varchar (50) NULL,

    CONSTRAINT PK_Inscription PRIMARY KEY (Medecin, Conference),
    CONSTRAINT FK_Commercial_Inscription FOREIGN KEY (Commercial) REFERENCES Commercial(Email),
    -- C3
    CONSTRAINT FK_Invitation FOREIGN KEY (Medecin, Conference) REFERENCES Invitation(Medecin, Conference)
);

-- ---------------------------------------------------------------------------------------------------------------------
-- Cr1
-- Trigger to check if the email is already used in the table Commercial
DROP TRIGGER IF EXISTS `check_emails_commercial`;
DELIMITER //
CREATE OR REPLACE TRIGGER `check_emails_commercial`
BEFORE INSERT ON `Commercial`
FOR EACH ROW

-- Check if the email is already used in table Commercial, Medecin or Technicien
BEGIN
    IF (SELECT COUNT(*) FROM `Commercial` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
    IF (SELECT COUNT(*) FROM `Medecin` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
    IF (SELECT COUNT(*) FROM `Technicien` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
END//
DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------
-- Cr1
-- Trigger to check if the email is already used in the table Technicien
DROP TRIGGER IF EXISTS `check_emails_technicien`;
DELIMITER //
CREATE OR REPLACE TRIGGER `check_emails_technicien`
BEFORE INSERT ON `Technicien`
FOR EACH ROW

-- Check if the email is already used in table Commercial, Medecin or Technicien
BEGIN
    IF (SELECT COUNT(*) FROM `Commercial` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
    IF (SELECT COUNT(*) FROM `Medecin` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
    IF (SELECT COUNT(*) FROM `Technicien` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
END//
DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------
-- Cr1
-- Trigger to check if the email is already used in the table Medecin
DROP TRIGGER IF EXISTS `check_emails_medecin`;
DELIMITER //
CREATE OR REPLACE TRIGGER `check_emails_medecin`
BEFORE INSERT ON `Medecin`
FOR EACH ROW

-- Check if the email is already used in table Commercial, Medecin or Technicien
BEGIN
    IF (SELECT COUNT(*) FROM `Commercial` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
    IF (SELECT COUNT(*) FROM `Medecin` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
    IF (SELECT COUNT(*) FROM `Technicien` WHERE `Email` = NEW.Email) > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email already used';
    END IF;
END//
DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------
-- C2
-- Trigger to check if the number of medecin in table inscription is lower than the number of medecin in table conference
DROP TRIGGER IF EXISTS `check_nb_medecin`;
DELIMITER //
CREATE OR REPLACE TRIGGER `check_nb_medecin`
BEFORE INSERT ON `Inscription`
FOR EACH ROW

-- Check if the number of medecin in table inscription is lower than the number of medecin in table conference
BEGIN

    IF (SELECT COUNT(*) FROM `Inscription` WHERE `Conference` = NEW.Conference) >= (SELECT NbMaxMedecin FROM `Conference` WHERE `PK_Conference` = NEW.Conference) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Number of medecin is too high';
    END IF;
END//
DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------
-- Cr2
-- Trigger to ensure that if FK_TechnicienResp is not null, FK_CommercialResp is null
DROP TRIGGER IF EXISTS `check_technicien_commercial`;
DELIMITER //
CREATE OR REPLACE TRIGGER `check_technicien_commercial`
BEFORE INSERT ON `Conference`
FOR EACH ROW

    -- Check if the Fk_TechnicienResp is exclusive with Fk_CommercialResp
BEGIN
    IF (NEW.TechnicienResp IS NOT NULL AND NEW.CommercialResp IS NOT NULL) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Technicien email and Commercial email cannot be the same';
    END IF;
END//
DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------


-- ---------------------------------------------------------------------------------------------------------------------
-- C4
-- Trigger to check the date of the inscription is one week before the date of the conference
DROP TRIGGER IF EXISTS `check_date_inscription`;
DELIMITER //
CREATE OR REPLACE TRIGGER `check_date_inscription`
BEFORE INSERT ON `Inscription`
FOR EACH ROW

-- Check the date of the iscription is one week before the date of the conference
BEGIN
    IF (NEW.DateInscription >= (SELECT DateDebut FROM `Conference` WHERE `PK_Conference` = NEW.Conference) - INTERVAL 7 DAY) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Date of the inscription is too late';
    END IF;
END//
DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------
-- C5 / Cr3
-- Trigger to check that the date of the conference is greater than CURDATE() + 7 days
DROP TRIGGER IF EXISTS `check_date_conference`;
DELIMITER //
CREATE OR REPLACE TRIGGER `check_date_conference`
BEFORE INSERT ON `Conference`
FOR EACH ROW

-- Check that the date of the conference is greater than CURDATE() + 7 days
BEGIN
    IF (NEW.DateDebut <= (CURDATE() + INTERVAL 7 DAY)) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The starting date of the conference is before today + 7 days';
    END IF;
END//
DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------


-- ---------------------------------------------------------------------------------------------------------------------
-- Cr4
-- Trigger to check if tha DateFin is greater than DateDebut + 7 days
-- DROP TRIGGER IF EXISTS `check_date_fin`;
-- DELIMITER //
-- CREATE OR REPLACE TRIGGER `check_date_fin`
-- BEFORE INSERT ON `Conference`
-- FOR EACH ROW

-- Check if tha DateFin is greater than DateDebut + 7 days
-- BEGIN
--     IF (NEW.DateFin <= NEW.DateDebut + INTERVAL 7 DAY) THEN
--         SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'End date of the conference is less than 7 days after the start date';
--     END IF;
-- END//
-- DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------
-- Views left join inviations
CREATE VIEW invitation_inscription AS
select Invitation.`Medecin`,`Invitation`.`Conference`,`Invitation`.`Commercial` AS `CommercialInv`,i1.`Commercial` AS `CommercialIns`,i1.`DateInscription`
FROM Invitation
LEFT JOIN Inscription i1
ON `i1`.`Medecin` = Invitation.`Medecin` and i1.`Conference` = Invitation.`Conference`;
-- ---------------------------------------------------------------------------------------------------------------------
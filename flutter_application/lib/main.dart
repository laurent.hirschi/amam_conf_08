import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_application/logic/viewModel/conf_view_model.dart';
import 'package:flutter_application/services/conf_api.dart';
import 'package:flutter_application/services/conf_prefs.dart';
import 'package:flutter_application/services/conf_repository.dart';
import 'package:flutter_application/services/conf_storage.dart';
import 'package:flutter_application/ui/view/add_conf_tech.dart';
import 'package:flutter_application/ui/view/add_conf_view.dart';
import 'package:flutter_application/ui/view/medecin_view/list_inscription_view.dart';
import 'package:flutter_application/ui/view/login_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application/ui/view/technician_view.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'logic/viewModel/login_view_model.dart';
import 'logic/viewModel/medecin_view_model.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var test = ConfStorage();
  final shared = await SharedPreferences.getInstance();
  final test2 = ConfSharedPreferences(plugin: shared);
  if (!kIsWeb) test.initDb();
  var ip = (!kIsWeb && Platform.isAndroid) ? "10.0.2.2:3000" : "127.0.0.1:3000";
  runApp(Conf(
    repository: ConfRepository(
      storage: test,
      api: ConfApi(baseUrl: ip),
      preferences: test2,
    ),
  ));
}

class Conf extends StatelessWidget {
  const Conf({Key? key, required this.repository}) : super(key: key);

  final ConfRepository repository;

  @override
  Widget build(BuildContext context) {
    return Provider.value(
      value: (context) => repository,
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => ConfViewModel(repository: repository),
          ),
          ChangeNotifierProvider(
            create: (_) => LoginViewModel(repository: repository),
          ),
          ChangeNotifierProvider(
            create: (_) => MedecinViewModel(repository: repository),
          ),
        ],
        child: MaterialApp(
          theme: ThemeData(
              primarySwatch: Colors.blue,
              visualDensity: VisualDensity.adaptivePlatformDensity),
          initialRoute: '/',
          routes: {
            '/': (context) => const LoginView(),
            '/technicianView': (context) => const TechnicianView(),
            '/listConf': (context) => const ListInscriptionView(),
            '/addConf': (context) => const AddConfView(),
            '/commercialView': (context) => const AddConfView(),
            '/addConfTech': (context) => const AddConfTechView(),
          },
        ),
      ),
    );
  }
}

class Conference {
  int pkConference;
  String? medecin;
  String? commercial;
  String titre;
  String? lieu;
  DateTime dateDebut;
  DateTime dateFin;
  String? dateInscription;
  String? techValidate;
  int? nbrInvites;
  int? nbrInscrits;

  Conference({
    required this.pkConference,
    this.medecin,
    this.commercial,
    required this.titre,
    this.lieu,
    required this.dateDebut,
    required this.dateFin,
    this.dateInscription,
    this.nbrInvites,
    this.nbrInscrits,
  });

  // Constructor for incoming JSON
  Conference.fromJson(Map<String, dynamic> json)
      : pkConference = json['PK_Conference'],
        medecin = json['Medecin'],
        commercial = json['Commercial'],
        titre = json['Titre'],
        lieu = json['Lieu'],
        dateDebut = DateTime.parse(json['DateDebut']),
        dateFin = DateTime.parse(json['DateFin']),
        dateInscription = json['DateInscription'],
        techValidate = json['TechnicienValide'],
        nbrInvites = json['NbrInvites'],
        nbrInscrits = json['NbrInscrits'];

  // Convert Conference to JSON
  Map<String, dynamic> toJson() => {
        'PK_Conference': pkConference,
        'Medecin': medecin,
        'Commercial': commercial,
        'Titre': titre,
        'Lieu': lieu,
        'DateDebut': dateDebut.toIso8601String(),
        'DateFin': dateFin.toIso8601String(),
        'DateInscription': dateInscription,
        "TechnicienValide": techValidate,
        "NbrInvites": nbrInvites,
        "NbrInscrits": nbrInscrits,
      };

  bool isValidate() {
    return techValidate != null;
  }

  String getStats() {
    return nbrInscrits != null && nbrInvites != null
        ? nbrInvites.toString() + "/" + nbrInscrits.toString()
        : "";
  }

  /*  int? id;
  String titre;
  DateTime dateDebut;
  DateTime dateFin;
  String? lieu;
  bool invited;
  Medecin? medecin; */
}

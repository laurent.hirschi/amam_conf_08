import 'package:flutter/material.dart';

import '../../services/conf_repository.dart';

class LoginViewModel extends ChangeNotifier {
  final ConfRepository repository;

  LoginViewModel({required this.repository});

  Future<int> login(String username, String password) async {
    return await repository.checkLogin(username, password);
  }
}

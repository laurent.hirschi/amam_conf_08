import 'package:flutter/material.dart';
import 'package:flutter_application/model/medecin.dart';
import 'package:flutter_application/services/conf_repository.dart';

enum MedecinStatus { loading, success, failure }

class MedecinViewModel extends ChangeNotifier {
  final List<Medecin> medecins;
  final ConfRepository repository;
  MedecinStatus status = MedecinStatus.loading;

  MedecinViewModel({required this.repository}) : medecins = [];

  void disposeValues() {
    medecins.clear();
  }

  void fetchMedecins() async {
    medecins.clear();
    medecins.addAll(await repository.getMedecins());
    status = MedecinStatus.success;
    print("aaaaaaaaa");
    print(medecins.length);
    notifyListeners();
  }
}

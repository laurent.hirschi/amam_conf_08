import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application/model/conference.dart';
import 'package:flutter_application/model/medecin.dart';
import 'package:flutter_application/model/stat_medecin.dart';
import 'package:flutter_application/services/conf_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum ConfStatus1 { loading, success, failure, isempty }

enum ConfStatus { loading, success, failure }

class ConfViewModel extends ChangeNotifier {
  final List<Medecin> medecins = [];
  final List<Conference> allConferences = [];
  final List<StatMedecin> stats = [];

  final ConfRepository repository;
  ConfStatus1 status = ConfStatus1.loading;
  ConfStatus addStatus = ConfStatus.loading;
  ConfStatus medStatus = ConfStatus.loading;
  ConfStatus confStatus = ConfStatus.loading;

  ConfViewModel({required this.repository});

  void disposeValues() async {
    _conferences.clear();
    status = ConfStatus1.loading;
    repository.shared.clear();
    repository.flush();
  }

  void insertConference(Conference conference, bool invitation) async {
    if (repository.shared.type == UserRoles.commercial.toString()) {
      conference.commercialResp = repository.shared.id!;
    } else if (repository.shared.type == UserRoles.technicien.toString()) {
      conference.technicienResp = repository.shared.id!;
    }
    if (!kIsWeb) {
      await repository.insertSQLConference(conference);
    }
    await repository.insertAPIConference(conference, invitation);
    allConferences.add(conference);
    addStatus = ConfStatus.success;
    notifyListeners();
  }

  void getMedecins() async {
    medecins.addAll(await repository.getMedecins());
    medStatus = ConfStatus.success;
    notifyListeners();
  }

  void invite(String pkConference, String email) async {
    await repository.inviteAPIConference(pkConference, email);
  }

  void getAllConferences() async {
    allConferences.clear();
    allConferences.addAll(await repository.getAPIAllConferences());
    confStatus = ConfStatus.success;
    notifyListeners();
  }

  void getStatsMedecin() async {
    stats.clear();
    stats.addAll(await repository.getAPIStatMedecin());
    confStatus = ConfStatus.success;
    notifyListeners();
  }

  void getConferencesInvitable() async {
    allConferences.clear();
    var list = await repository.getAPIAllConferences();
    for (var conf in list) {
      if (conf.commercialInv == null) {
        allConferences.add(conf);
      }
    }
    confStatus = ConfStatus.success;
    notifyListeners();
  }

  void validateConference(int id, bool validate) async {
    await repository.updateValidationAPIConference(id, validate);
    getAllConferences();
  }

  /* --- Liste Inscription and Invitation functions ------------------- */

  final List<Conference> _conferences = [];

  List<Conference> get conferences {
    return _conferences;
  }

  void fetchConferences(bool online) async {
    _conferences.clear();
    // Web
    if (kIsWeb) {
      print("Web: fetching online");
      // fetch data online
      var result = await repository.getAPIConferences();
      // load conferences
      _conferences.addAll(result);
      // status change
      status = ConfStatus1.success;
      notifyListeners();
      return;
    }
    // fetch data locally
    var local = await repository.getSQLConferences();
    // if empty do getInvitations
    // else load conferences
    if (local.isNotEmpty && !online) {
      print("fetching locally");
      _conferences.addAll(local);
    } else {
      print("fetching online");
      // fetch data online
      var result = await repository.getAPIConferences();
      // load conferences
      print(result[0]);
      _conferences.addAll(result);
      // save conferences locally
      if (_conferences.isNotEmpty) {
        for (var i = 0; i < _conferences.length; i++) {
          await repository.insertSQLConference(_conferences[i]);
        }
      } else {
        status = ConfStatus1.isempty;
        notifyListeners();
        return;
      }
    }
    status = ConfStatus1.success;
    notifyListeners();
  }

  Future<void> rejectInvitation(Conference conf, bool delete) async {
    if (delete) {
      _deleteInvitation(conf);
    } else {
      conf.dateInscription = null;
      var result =
          await repository.rejectInvitation(conf.convert2Inscription());
      if (result) {
        _conferences.remove(conf);
        _conferences.add(conf);
        notifyListeners();
      }
    }
  }

  Future<void> acceptInvitation(Conference conf) async {
    conf.dateInscription = DateTime.now();
    conf.commercialIns = null;
    var result = await repository.acceptInvitation(conf.convert2Inscription());
    if (result) {
      _conferences.remove(conf);
      _conferences.add(conf);
      notifyListeners();
    }
  }

  Future<void> _deleteInvitation(Conference conf) async {
    var result = await repository.deleteInvitation(conf.convert2Invitation());
    if (result) {
      _conferences.remove(conf);
      notifyListeners();
    }
  }

  Future<String> retryChanges() async {
    print("ViewModel: Retry Changes");
    return await repository.retryInvitationChanges();
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_application/model/medecin.dart';
import 'package:flutter_application/ui/view/stats_grid_view.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../model/conference.dart';
import '../../logic/viewModel/conf_view_model.dart';
import 'invite_view.dart';
import 'list_medecin_view.dart';

class AddConfView extends StatefulWidget {
  const AddConfView({Key? key}) : super(key: key);

  @override
  State<AddConfView> createState() => _AddConfViewState();
}

class _AddConfViewState extends State<AddConfView> {
  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Consumer<ConfViewModel>(
      builder: (context, confViewModel, _) {
        const title = [
          'Ajouter une conférence',
          'Inviter medecin',
          'Statistiques conférences',
        ];
        return Scaffold(
            appBar: AppBar(
              title: Text(title[_currentIndex]),
              actions: [
                IconButton(
                  icon: const Icon(Icons.refresh),
                  onPressed: () {
                    print("refresh");
                    context.read<ConfViewModel>().getAllConferences();
                  },
                ),
              ],
            ),
            bottomNavigationBar: BottomNavigationBar(
              onTap: onTabTapped,
              currentIndex:
                  _currentIndex, // this will be set when a new tab is tapped
              items: const [
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'Ajouter',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.message),
                  label: 'Inviter',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.bar_chart),
                  label: 'Statistiques',
                ),
              ],
            ),
            body: navigateTo(confViewModel));
      },
    ), onWillPop: () async {
      //Reset values in Provider before going back
      Provider.of<ConfViewModel>(context, listen: false).disposeValues();
      Navigator.pop(context, true);
      return true;
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  navigateTo(confViewModel) {
    switch (_currentIndex) {
      case 0:
        return AddViewSuccess(
          confViewModel: confViewModel,
        );
      case 1:
        return InviteView(
          confViewModel: confViewModel,
        );
      case 2:
        return StatsGridView(
          confViewModel: confViewModel,
        );
    }
  }
}

class StatsView {}

class DatePickerField extends StatefulWidget {
  DatePickerField({
    Key? key,
    required this.title,
    this.controller,
    required this.valid,
  }) : super(key: key);

  final String title;
  final TextEditingController? controller;
  final DateFormat df = DateFormat("dd.MM.yyyy – kk:mm");
  final String? Function(String?) valid;

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePickerField> {
  DateTime selectedDate = DateTime.now();
  @override
  Widget build(BuildContext buildContext) {
    return TextFormField(
      controller: widget.controller,
      readOnly: true,
      decoration: InputDecoration(
        prefixIcon: const Icon(Icons.calendar_month),
        label: Text(widget.title),
        border: const OutlineInputBorder(),
      ),
      validator: widget.valid,
      onTap: () async {
        DateTime now = DateTime.now();
        DateTime? pickedDate = await showDatePicker(
            context: context,
            initialDate: DateTime(now.year, now.month, now.day, now.hour, 0)
                .add(const Duration(days: 7)),
            firstDate: DateTime(now.year, now.month, now.day, now.hour, 0)
                .add(const Duration(days: 7)),
            lastDate: DateTime(2100));
        if (pickedDate != null) {
          TimeOfDay? pickedTime = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.now(),
          );
          if (pickedTime != null) {
            DateTime dateTime = DateTime(pickedDate.year, pickedDate.month,
                pickedDate.day, pickedTime.hour, pickedTime.minute);
            widget.controller?.text = widget.df.format(dateTime);
          }
        }
      },
    );
  }
}

class AddViewSuccess extends StatefulWidget {
  const AddViewSuccess({Key? key, required this.confViewModel})
      : super(key: key);

  final ConfViewModel confViewModel;

  @override
  State<AddViewSuccess> createState() => _AddViewSuccessState();
}

class _AddViewSuccessState extends State<AddViewSuccess> {
  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final startDateController = TextEditingController();
  final endDateController = TextEditingController();
  final locationController = TextEditingController();
  DateFormat df = DateFormat("dd.MM.yyyy – kk:mm");
  final medecinController = TextEditingController();
  Medecin? medecinToInvite;

  @override
  Widget build(BuildContext context) {
    if (widget.confViewModel.addStatus == ConfStatus.success) {
      showSnackBar("Conference ajouté ! ", context);
    } else if (widget.confViewModel.addStatus == ConfStatus.failure) {
      showSnackBar("Erreur lors de l'ajout ! ", context);
    }

    widget.confViewModel.addStatus = ConfStatus.loading;
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(30),
        child: Form(
          key: _formKey,
          child: Wrap(
            runSpacing: 30,
            children: [
              TextFormField(
                controller: titleController,
                readOnly: false,
                obscureText: false,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.title),
                  label: Text("Titre"),
                  border: OutlineInputBorder(),
                ),
                validator: (value) => (value == null || value.isEmpty)
                    ? 'Veuillez renseigner le champ'
                    : null,
              ),
              DatePickerField(
                title: 'Date début',
                controller: startDateController,
                valid: (value) => (value == null || value.isEmpty)
                    ? 'Veuillez renseigner le champ'
                    : null,
              ),
              DatePickerField(
                title: 'Date fin',
                controller: endDateController,
                valid: (value) => (value == null || value.isEmpty)
                    ? 'Veuillez renseigner le champ'
                    : null,
              ),
              TextFormField(
                controller: locationController,
                readOnly: false,
                obscureText: false,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.pin_drop_rounded),
                  label: Text("Lieu"),
                  border: OutlineInputBorder(),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _navigateAndDisplaySelection(context);
                },
                child: AbsorbPointer(
                  child: TextFormField(
                    controller: medecinController,
                    readOnly: true,
                    obscureText: false,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.person),
                      label: Text('Medecin'),
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: ElevatedButton(
                  child: const Text("Ajouter"),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      var startDate = df.parse(startDateController.text);
                      var endDate = df.parse(endDateController.text);
                      if (startDate.compareTo(endDate) < 0) {
                        widget.confViewModel.insertConference(
                          Conference(
                              pkConference: -1,
                              titre: titleController.text,
                              dateDebut: startDate,
                              dateFin: endDate,
                              lieu: locationController.text,
                              medecin: (medecinController.text == "")
                                  ? null
                                  : medecinController.text,
                              written: 0),
                          (medecinController.text == "") ? false : true,
                        );
                        startDateController.clear();
                        endDateController.clear();
                        titleController.clear();
                        locationController.clear();
                        medecinController.clear();
                      } else {
                        startDateController.clear();
                        endDateController.clear();
                        showSnackBar(
                            "Erreur lors de l'ajout, verifiez les dates",
                            context);
                      }
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showSnackBar(String message, context) {
    Future.delayed(Duration.zero, () {
      final snackBar = SnackBar(
        content: Text(message),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  void _navigateAndDisplaySelection(BuildContext context) async {
    medecinToInvite = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const ListMedView()),
    );
    if (medecinToInvite != null) {
      medecinController.text = medecinToInvite!.email;
    } else {
      medecinController.text = "";
    }
  }
}

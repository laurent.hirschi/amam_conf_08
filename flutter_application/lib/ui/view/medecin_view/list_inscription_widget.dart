import 'package:flutter/material.dart';
import 'package:flutter_application/logic/viewModel/conf_view_model.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class InscriptionList extends StatefulWidget {
  const InscriptionList({
    Key? key,
  }) : super(key: key);

  @override
  State<InscriptionList> createState() => _InscriptionListState();
}

class _InscriptionListState extends State<InscriptionList> {
  @override
  Widget build(BuildContext context) {
    var df = DateFormat("dd MMMM à kk:mm");
    final confViewModel = context.watch<ConfViewModel>();
    final conferences = confViewModel.conferences;
    return ListView.builder(
      itemCount: conferences.length,
      itemBuilder: (context, index) {
        return Card(
          child: ListTile(
            leading: const CircleAvatar(
              child: Icon(Icons.people),
            ),
            title: Text(
              conferences[index].titre,
              style: const TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              conferences[index].lieu == null
                  ? df.format(conferences[index].dateDebut)
                  : conferences[index].lieu! +
                      ", " +
                      df.format(conferences[index].dateDebut),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: (conferences[index].dateInscription == null)
                  ? [
                      IconButton(
                        icon: const Icon(
                          Icons.done,
                          color: Colors.green,
                        ),
                        onPressed: () {
                          confViewModel.acceptInvitation(
                            conferences[index],
                          );
                        },
                      ),
                      IconButton(
                        icon: const Icon(
                          Icons.close,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          confViewModel.rejectInvitation(
                            conferences[index],
                            true,
                          );
                        },
                      ),
                    ]
                  : [
                      IconButton(
                        icon: const Icon(
                          Icons.close,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          confViewModel.rejectInvitation(
                            conferences[index],
                            false,
                          );
                        },
                      ),
                    ],
            ),
          ),
        );
      },
    );
  }
}

// ignore_for_file: prefer_const_constructors_in_immutables

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_application/model/conference.dart';
import 'package:flutter_application/logic/viewModel/conf_view_model.dart';
import 'package:flutter_application/model/stat_medecin.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:syncfusion_flutter_datagrid_export/export.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class MedStatWidget extends StatefulWidget {
  MedStatWidget({Key? key, confViewModel}) : super(key: key);

  @override
  State<MedStatWidget> createState() => _MedStatWidgetState();
}

class _MedStatWidgetState extends State<MedStatWidget> {
  late StatMedecinDataSource _statDataSource;
  List<StatMedecin> _stats = <StatMedecin>[];
  final _key = GlobalKey<SfDataGridState>();

  Future<void> exportDataGridToPdf() async {
    final Directory directory = await getApplicationDocumentsDirectory();
    print(directory);
    final PdfDocument document =
        _key.currentState!.exportToPdfDocument(fitAllColumnsInOnePage: true);
    final List<int> bytes = document.save();
    File('${directory.path}/ConfStats.pdf').writeAsBytes(bytes);
    document.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final confViewModel = context.watch<ConfViewModel>();
    _stats = confViewModel.stats;
    _statDataSource = StatMedecinDataSource(conferences: _stats);
    switch (confViewModel.confStatus) {
      case ConfStatus.failure:
        return const Scaffold(body: Text("data"));
      case ConfStatus.loading:
        confViewModel.getStatsMedecin();
        return const Center(
          child: CircularProgressIndicator(),
        );
      case ConfStatus.success:
        return Column(
          children: <Widget>[
            ElevatedButton(
                onPressed: () {
                  exportDataGridToPdf();
                },
                child: const Text("export")),
            Expanded(
              child: SfDataGrid(
                key: _key,
                source: _statDataSource,
                columnWidthMode: ColumnWidthMode.fill,
                columns: <GridColumn>[
                  GridColumn(
                      columnName: 'ID',
                      label: Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          child: const Text(
                            'ID',
                          ))),
                  GridColumn(
                      columnName: 'NbrInscriptions',
                      label: Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          child: const Text('Inscriptions'))),
                  GridColumn(
                      columnName: 'NbrInvitations',
                      label: Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          child: const Text('Invitations'))),
                ],
              ),
            ),
          ],
        );
    }
  }
}

class StatMedecinDataSource extends DataGridSource {
  StatMedecinDataSource({required List<StatMedecin> conferences}) {
    dataGridRows = conferences
        .map<DataGridRow>((dataGridRow) => DataGridRow(cells: [
              DataGridCell<String>(
                  columnName: 'ID', value: dataGridRow.medecin),
              DataGridCell<String>(
                  columnName: 'NbrInscriptions',
                  value: dataGridRow.nbInscriptions.toString()),
              DataGridCell<String>(
                  columnName: 'NbrInvitations',
                  value: dataGridRow.nbInvitations.toString()),
            ]))
        .toList();
  }

  List<DataGridRow> dataGridRows = [];

  @override
  List<DataGridRow> get rows => dataGridRows;

  @override
  DataGridRowAdapter? buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((dataGridCell) {
      return Container(
          alignment: (dataGridCell.columnName == 'id' ||
                  dataGridCell.columnName == 'Datefin')
              ? Alignment.centerRight
              : Alignment.centerLeft,
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Text(
            dataGridCell.value.toString(),
            overflow: TextOverflow.ellipsis,
          ));
    }).toList());
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_application/ui/view/medecin_view/list_inscription_widget.dart';
import 'package:flutter_application/ui/view/medecin_view/med_stat_widget.dart';
import 'package:intl/intl.dart';
import '../../../logic/viewModel/conf_view_model.dart';
import 'package:provider/provider.dart';

class ListInscriptionView extends StatefulWidget {
  const ListInscriptionView({Key? key}) : super(key: key);

  @override
  State<ListInscriptionView> createState() => _ListInscriptionViewState();
}

class _ListInscriptionViewState extends State<ListInscriptionView> {
  DateFormat df = DateFormat("dd.MM.yyyy – kk:mm");
  var _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Consumer<ConfViewModel>(
      builder: (context, confViewModel, _) {
        const title = 'Liste des invitations';
        var myActions = [
          [
            IconButton(
              icon: const Icon(Icons.refresh),
              onPressed: () async {
                // do method retry
                var result = await confViewModel.retryChanges();
                if (result == "SUCCESS") {
                  confViewModel.fetchConferences(true);
                } else {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text("conflits"),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'OK'),
                            child: const Text('OK'),
                          ),
                        ],
                      );
                    },
                  );
                }
              },
            )
          ],
          null
        ];
        switch (confViewModel.status) {
          case ConfStatus1.isempty:
            return Scaffold(
              appBar: AppBar(
                title: const Text(title),
              ),
              body: const Center(
                child: Text("You are not invited to any conference"),
              ),
            );
          case ConfStatus1.failure:
            return const Scaffold(
              body: Center(
                child: Text("Error"),
              ),
            );
          case ConfStatus1.loading:
            confViewModel.fetchConferences(false);
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          case ConfStatus1.success:
            return Scaffold(
              appBar: AppBar(
                title: const Text(title),
                actions: myActions[_currentIndex],
              ),
              bottomNavigationBar: BottomNavigationBar(
                onTap: onTabTapped,
                currentIndex:
                    _currentIndex, // this will be set when a new tab is tapped
                items: const [
                  BottomNavigationBarItem(
                    icon: Icon(Icons.home),
                    label: 'Inscriptions',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.bar_chart),
                    label: 'Statistiques',
                  ),
                ],
              ),
              body: navigateTo(confViewModel),
              //body: const InscriptionList(),
            );
        }
      },
    ), onWillPop: () async {
      //Reset values in Provider before going back
      Provider.of<ConfViewModel>(context, listen: false).disposeValues();
      Navigator.pop(context, true);
      return true;
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  navigateTo(confViewModel) {
    switch (_currentIndex) {
      case 0:
        return const InscriptionList();
      case 1:
        return MedStatWidget();
    }
  }
}

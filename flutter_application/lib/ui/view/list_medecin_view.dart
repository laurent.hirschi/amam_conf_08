import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../logic/viewModel/conf_view_model.dart';

class ListMedView extends StatefulWidget {
  const ListMedView({Key? key}) : super(key: key);

  @override
  State<ListMedView> createState() => _ListMedViewState();
}

class _ListMedViewState extends State<ListMedView> {
  int selectedItem = -1;

  @override
  Widget build(BuildContext context) {
    final confViewModel = context.watch<ConfViewModel>();

    const title = 'Liste des medecins';
    switch (confViewModel.medStatus) {
      case ConfStatus.failure:
        return const Scaffold(body: Text("data"));
      case ConfStatus.loading:
        confViewModel.getMedecins();
        return const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      case ConfStatus.success:
        return Scaffold(
          appBar: const ListMedecinAppBar(
            title: title,
          ),
          body: ListView.builder(
            itemCount: confViewModel.medecins.length,
            itemBuilder: (context, index) {
              return Container(
                color: (selectedItem == index)
                    ? Colors.blue.withOpacity(0.5)
                    : Colors.transparent,
                child: ListTile(
                  title: Text(confViewModel.medecins[index].email),
                  onTap: () {
                    setState(() {
                      selectedItem = index;
                    });
                    Navigator.pop(context, confViewModel.medecins[index]);
                  },
                ),
              );
            },
          ),
        );
    }
  }
}

class ListMedecinAppBar extends StatelessWidget implements PreferredSizeWidget {
  const ListMedecinAppBar({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Size get preferredSize => const Size.fromHeight(50);

  @override
  AppBar build(BuildContext context) {
    return AppBar(
      title: Text(title),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../logic/viewModel/conf_view_model.dart';
import 'add_conf_tech.dart';

class TechnicianView extends StatefulWidget {
  const TechnicianView({Key? key}) : super(key: key);

  @override
  State<TechnicianView> createState() => _TechnicianViewState();
}

class _TechnicianViewState extends State<TechnicianView> {
  int selectedItem = -1;

  @override
  Widget build(BuildContext context) {
    final confViewModel = context.watch<ConfViewModel>();
    DateFormat df = DateFormat("dd.MM.yyyy – kk:mm");
    const title = 'Validation de conferences';
    switch (confViewModel.confStatus) {
      case ConfStatus.failure:
        return const Scaffold(body: Text("data"));
      case ConfStatus.loading:
        confViewModel.getAllConferences();
        return const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      case ConfStatus.success:
        return Scaffold(
          appBar: AppBar(
            title: Text(title),
            actions: [
              IconButton(
                  icon: const Icon(Icons.add),
                  onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const AddConfTechView()),
                      ))
            ],
          ),
          body: ListView.builder(
            itemCount: confViewModel.allConferences.length,
            itemBuilder: (context, index) {
              var conf = confViewModel.allConferences[index];
              return Container(
                color: (selectedItem == index)
                    ? Colors.blue.withOpacity(0.5)
                    : Colors.transparent,
                child: ListTile(
                  title: Text(conf.titre),
                  subtitle: Text(df.format(conf.dateDebut)),
                  trailing: SizedBox(
                    width: 40,
                    child: IconButton(
                        onPressed: () {
                          confViewModel.validateConference(
                              conf.pkConference, !conf.isValidate());
                        },
                        icon: Icon(Icons.done,
                            color: conf.isValidate()
                                ? Colors.green
                                : Colors.grey)),
                  ),
                  onTap: () {
                    setState(() {
                      selectedItem = index;
                    });
                    Navigator.pop(context, conf);
                  },
                ),
              );
            },
          ),
        );
    }
  }
}

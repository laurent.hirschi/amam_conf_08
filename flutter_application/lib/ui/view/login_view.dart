import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../logic/viewModel/login_view_model.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  var usernameController = TextEditingController();

  var passwordController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    passwordController.dispose();
    usernameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final loginViewModel = context.watch<LoginViewModel>();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Page de login"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60, bottom: 30),
              child: Center(
                child: SizedBox(
                  width: 200,
                  height: 150,
                  child: Image.asset('assets/logo.png'),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: usernameController,
                readOnly: false,
                obscureText: false,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.person),
                  label: Text("Username"),
                  border: OutlineInputBorder(),
                ),
                validator: (value) => (value == null || value.isEmpty)
                    ? 'Veuillez renseigner le champ'
                    : null,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15, right: 15, top: 15, bottom: 30),
              child: TextFormField(
                controller: passwordController,
                readOnly: false,
                obscureText: true,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.person),
                  label: Text("Password"),
                  border: OutlineInputBorder(),
                ),
                validator: (value) => (value == null || value.isEmpty)
                    ? 'Veuillez renseigner le champ'
                    : null,
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                int result = await loginViewModel.login(
                  usernameController.text,
                  passwordController.text,
                );
                usernameController.clear();
                passwordController.clear();
                var navigation = "/";
                switch (result) {
                  case 1:
                    navigation = "/technicianView";
                    break;
                  case 2:
                    navigation = "/commercialView";
                    break;
                  case 3:
                    navigation = "/listConf";
                    break;
                  default:
                }
                Navigator.pushNamed(context, navigation);
              },
              child: const Text(
                'Login',
              ),
            ),
            const SizedBox(
              height: 130,
            ),
          ],
        ),
      ),
    );
  }
}

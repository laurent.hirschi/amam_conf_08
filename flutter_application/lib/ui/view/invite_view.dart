import 'package:flutter/material.dart';
import 'package:flutter_application/logic/viewModel/conf_view_model.dart';
import 'package:flutter_application/ui/view/list_medecin_view.dart';

import '../../model/conference.dart';
import '../../model/medecin.dart';
import 'list_conference_view.dart';

class InviteView extends StatefulWidget {
  const InviteView({Key? key, required this.confViewModel}) : super(key: key);

  final ConfViewModel confViewModel;

  @override
  State<InviteView> createState() => _InviteViewState();
}

class _InviteViewState extends State<InviteView> {
  final medecinController = TextEditingController();
  final conferenceController = TextEditingController();

  Medecin? medecinToInvite;
  Conference? confToInvite;
  final _formKey = GlobalKey<FormState>();

  void _navigateAndDisplaySelection(BuildContext context) async {
    medecinToInvite = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const ListMedView()),
    );
    if (medecinToInvite != null) {
      medecinController.text = medecinToInvite!.email;
    } else {
      medecinController.text = "";
    }
  }

  void _navigateAndDisplaySelection2(BuildContext context) async {
    confToInvite = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const ListConferenceView()),
    );
    if (confToInvite != null) {
      conferenceController.text = confToInvite!.pkConference.toString();
    } else {
      conferenceController.text = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(30),
        child: Form(
          key: _formKey,
          child: Wrap(
            runSpacing: 30,
            children: [
              GestureDetector(
                onTap: () {
                  _navigateAndDisplaySelection(context);
                },
                child: AbsorbPointer(
                  child: TextFormField(
                    controller: medecinController,
                    readOnly: true,
                    obscureText: false,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.person),
                      label: Text('Medecin'),
                      border: OutlineInputBorder(),
                    ),
                    validator: (value) => (value == null || value.isEmpty)
                        ? 'Veuillez renseigner le champ'
                        : null,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _navigateAndDisplaySelection2(context);
                },
                child: AbsorbPointer(
                  child: TextFormField(
                    controller: conferenceController,
                    readOnly: true,
                    obscureText: false,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.person),
                      label: Text('Conference'),
                      border: OutlineInputBorder(),
                    ),
                    validator: (value) => (value == null || value.isEmpty)
                        ? 'Veuillez renseigner le champ'
                        : null,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: ElevatedButton(
                  child: const Text("Inviter"),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      widget.confViewModel.invite(
                          conferenceController.text, medecinController.text);
                      conferenceController.clear();
                      medecinController.clear();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

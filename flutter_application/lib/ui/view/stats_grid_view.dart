// ignore_for_file: prefer_const_constructors_in_immutables

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_application/model/conference.dart';
import 'package:flutter_application/logic/viewModel/conf_view_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:syncfusion_flutter_datagrid_export/export.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class StatsGridView extends StatefulWidget {
  StatsGridView({Key? key, confViewModel}) : super(key: key);

  @override
  State<StatsGridView> createState() => _StatsGridViewState();
}

class _StatsGridViewState extends State<StatsGridView> {
  late ConferenceDataSource _confDataSource;
  List<Conference> _conferences = <Conference>[];
  final _key = GlobalKey<SfDataGridState>();

  Future<void> exportDataGridToPdf() async {
    final Directory directory = await getApplicationDocumentsDirectory();
    print(directory);
    final PdfDocument document =
        _key.currentState!.exportToPdfDocument(fitAllColumnsInOnePage: true);
    final List<int> bytes = document.save();
    File('${directory.path}/ConfStats.pdf').writeAsBytes(bytes);
    document.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final confViewModel = context.watch<ConfViewModel>();
    _conferences = confViewModel.allConferences;
    _confDataSource = ConferenceDataSource(conferences: _conferences);
    switch (confViewModel.confStatus) {
      case ConfStatus.failure:
        return const Scaffold(body: Text("data"));
      case ConfStatus.loading:
        confViewModel.getAllConferences();
        return const Center(
          child: CircularProgressIndicator(),
        );
      case ConfStatus.success:
        return Column(
          children: <Widget>[
            ElevatedButton(
                onPressed: () {
                  exportDataGridToPdf();
                },
                child: const Text("export")),
            Expanded(
              child: SfDataGrid(
                key: _key,
                source: _confDataSource,
                columnWidthMode: ColumnWidthMode.fill,
                columns: <GridColumn>[
                  GridColumn(
                      columnName: 'ID',
                      label: Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          child: const Text(
                            'ID',
                          ))),
                  GridColumn(
                      columnName: 'Titre',
                      label: Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          child: const Text('Titre'))),
                  GridColumn(
                      columnName: 'NbrInscrits',
                      label: Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          child: const Text('Inscrits'))),
                  GridColumn(
                      columnName: 'NbrInvites',
                      label: Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          child: const Text('Invités'))),
                ],
              ),
            ),
          ],
        );
    }
  }
}

class ConferenceDataSource extends DataGridSource {
  ConferenceDataSource({required List<Conference> conferences}) {
    dataGridRows = conferences
        .map<DataGridRow>((dataGridRow) => DataGridRow(cells: [
              DataGridCell<int>(
                  columnName: 'ID', value: dataGridRow.pkConference),
              DataGridCell<String>(
                  columnName: 'Titre', value: dataGridRow.titre),
              // DataGridCell<String>(
              //     columnName: 'Datedebut',
              //     value: dataGridRow.dateDebut.toString()),
              // DataGridCell<String>(
              //     columnName: 'Datefin', value: dataGridRow.dateFin.toString()),
              DataGridCell<String>(
                  columnName: 'NbrInscrits',
                  value: dataGridRow.nbrInscrits.toString()),
              DataGridCell<String>(
                  columnName: 'NbrInvites',
                  value: dataGridRow.nbrInvites.toString()),
            ]))
        .toList();
  }

  List<DataGridRow> dataGridRows = [];

  @override
  List<DataGridRow> get rows => dataGridRows;

  @override
  DataGridRowAdapter? buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((dataGridCell) {
      return Container(
          alignment: (dataGridCell.columnName == 'id' ||
                  dataGridCell.columnName == 'Datefin')
              ? Alignment.centerRight
              : Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Text(
            dataGridCell.value.toString(),
            overflow: TextOverflow.ellipsis,
          ));
    }).toList());
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../logic/viewModel/conf_view_model.dart';

class ListConferenceView extends StatefulWidget {
  const ListConferenceView({Key? key}) : super(key: key);

  @override
  State<ListConferenceView> createState() => _ListConferenceViewState();
}

class _ListConferenceViewState extends State<ListConferenceView> {
  int selectedItem = -1;

  @override
  Widget build(BuildContext context) {
    final confViewModel = context.watch<ConfViewModel>();

    const title = 'Liste des conferences';
    switch (confViewModel.confStatus) {
      case ConfStatus.failure:
        return const Scaffold(body: Text("data"));
      case ConfStatus.loading:
        confViewModel.getConferencesInvitable();
        return const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      case ConfStatus.success:
        return WillPopScope(child: Consumer<ConfViewModel>(
          builder: (context, confViewModel, _) {
            return Scaffold(
              appBar: const ListConfViewAppBar(
                title: title,
              ),
              body: ListView.builder(
                itemCount: confViewModel.allConferences.length,
                itemBuilder: (context, index) {
                  return Container(
                    color: (selectedItem == index)
                        ? Colors.blue.withOpacity(0.5)
                        : Colors.transparent,
                    child: ListTile(
                      title: Text(confViewModel.allConferences[index].titre),
                      onTap: () {
                        setState(() {
                          selectedItem = index;
                        });
                        Navigator.pop(
                            context, confViewModel.allConferences[index]);
                      },
                    ),
                  );
                },
              ),
            );
          },
        ), onWillPop: () async {
          //Reset values in Provider before going back
          Provider.of<ConfViewModel>(context, listen: false).disposeValues();
          Navigator.pop(context, true);
          return true;
        });
    }
  }
}

class ListConfViewAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  const ListConfViewAppBar({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Size get preferredSize => const Size.fromHeight(50);

  @override
  AppBar build(BuildContext context) {
    return AppBar(
      title: Text(title),
    );
  }
}

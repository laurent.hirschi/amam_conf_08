import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../model/conference.dart';
import '../../logic/viewModel/conf_view_model.dart';

class AddConfTechView extends StatefulWidget {
  const AddConfTechView({Key? key}) : super(key: key);

  @override
  State<AddConfTechView> createState() => _AddConfTechViewState();
}

class _AddConfTechViewState extends State<AddConfTechView> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Consumer<ConfViewModel>(
      builder: (context, confViewModel, _) {
        const title = 'Ajouter une conférence';
        return Scaffold(
          appBar: AppBar(
            title: const Text(title),
          ),
          body: AddConfTechViewSuccess(
            confViewModel: confViewModel,
          ),
        );
      },
    ), onWillPop: () async {
      //Reset values in Provider before going back
      Provider.of<ConfViewModel>(context, listen: false).disposeValues();
      Navigator.pop(context, true);
      return true;
    });
  }
}

class DatePickerField extends StatefulWidget {
  DatePickerField({
    Key? key,
    required this.title,
    this.controller,
  }) : super(key: key);

  final String title;
  final TextEditingController? controller;
  final DateFormat df = DateFormat("dd.MM.yyyy – kk:mm");

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePickerField> {
  DateTime selectedDate = DateTime.now();
  @override
  Widget build(BuildContext buildContext) {
    return TextFormField(
      controller: widget.controller,
      readOnly: true,
      decoration: InputDecoration(
        prefixIcon: const Icon(Icons.calendar_month),
        label: Text(widget.title),
        border: const OutlineInputBorder(),
      ),
      validator: (value) => (value == null || value.isEmpty)
          ? 'Veuillez renseigner le champ'
          : null,
      onTap: () async {
        DateTime now = DateTime.now();
        DateTime? pickedDate = await showDatePicker(
            context: context,
            initialDate:
                DateTime(now.year, now.month, now.day, now.hour + 1, 0),
            firstDate: DateTime.now(),
            lastDate: DateTime(2100));
        if (pickedDate != null) {
          TimeOfDay? pickedTime = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.now(),
          );
          if (pickedTime != null) {
            DateTime dateTime = DateTime(pickedDate.year, pickedDate.month,
                pickedDate.day, pickedTime.hour, pickedTime.minute);
            widget.controller?.text = widget.df.format(dateTime);
          }
        }
      },
    );
  }
}

class AddConfTechViewSuccess extends StatefulWidget {
  const AddConfTechViewSuccess({Key? key, required this.confViewModel})
      : super(key: key);

  final ConfViewModel confViewModel;

  @override
  State<AddConfTechViewSuccess> createState() => _AddConfTechViewSuccessState();
}

class _AddConfTechViewSuccessState extends State<AddConfTechViewSuccess> {
  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final startDateController = TextEditingController();
  final endDateController = TextEditingController();
  final locationController = TextEditingController();
  DateFormat df = DateFormat("dd.MM.yyyy – kk:mm");

  @override
  Widget build(BuildContext context) {
    if (widget.confViewModel.addStatus == ConfStatus.success) {
      showSnackBar("Conference ajouté ! ", context);
    } else if (widget.confViewModel.addStatus == ConfStatus.failure) {
      showSnackBar("Erreur lors de l'ajout ! ", context);
    }
    widget.confViewModel.addStatus = ConfStatus.loading;
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(30),
        child: Form(
          key: _formKey,
          child: Wrap(
            runSpacing: 30,
            children: [
              TextFormField(
                controller: titleController,
                readOnly: false,
                obscureText: false,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.title),
                  label: Text("Titre"),
                  border: OutlineInputBorder(),
                ),
                validator: (value) => (value == null || value.isEmpty)
                    ? 'Veuillez renseigner le champ'
                    : null,
              ),
              DatePickerField(
                title: 'Date début',
                controller: startDateController,
              ),
              DatePickerField(
                title: 'Date fin',
                controller: endDateController,
              ),
              TextFormField(
                controller: locationController,
                readOnly: false,
                obscureText: false,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.pin_drop_rounded),
                  label: Text("Lieu"),
                  border: OutlineInputBorder(),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: ElevatedButton(
                  child: const Text("Ajouter"),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      var startDate = df.parse(startDateController.text);
                      var endDate = df.parse(endDateController.text);
                      if (startDate.compareTo(endDate) < 0) {
                        widget.confViewModel.insertConference(
                          Conference(
                              pkConference: -1,
                              titre: titleController.text,
                              dateDebut: startDate,
                              dateFin: endDate,
                              lieu: locationController.text,
                              written: 0),
                          false,
                        );
                        startDateController.clear();
                        endDateController.clear();
                        titleController.clear();
                        locationController.clear();
                      } else {
                        startDateController.clear();
                        endDateController.clear();
                        showSnackBar(
                            "Erreur lors de l'ajout, verifiez les dates",
                            context);
                      }
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showSnackBar(String message, context) {
    Future.delayed(Duration.zero, () {
      final snackBar = SnackBar(
        content: Text(message),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }
}

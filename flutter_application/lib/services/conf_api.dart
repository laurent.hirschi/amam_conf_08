import 'package:flutter_application/model/conference.dart';
import 'package:flutter_application/model/inscription.dart';
import 'package:flutter_application/model/invitation.dart';
import 'package:flutter_application/model/login_message.dart';
import 'package:flutter_application/model/medecin.dart';
import 'package:flutter_application/model/stat_medecin.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ConfRequestFailure implements Exception {}

class ConfNotFound implements Exception {}

class ConfApi {
  final http.Client _httpClient = http.Client();
  //static const _baseUrl = '127.0.0.1:3000';
  final _baseUrl;
  static const String _pathListMedecin = 'medecin';
  static const String _pathListConf = 'conference';
  static const String _pathListMedecinConf = 'medecinconference';
  static const String _pathAddConf = 'addconference';
  static const String _pathInvConf = 'addInvitation';
  static const String _pathValidateConf = "validateconference";
  static const String _pathCheckLogin = 'login';
  static const String _pathGetConferences = 'getConferencesById';
  static const String _pathAddInscription = 'addInscription';
  static const String _pathRemoveInscription = 'removeInscription';
  static const String _pathRemoveInvitation = 'removeInvitation';
  static const String _pathRemoveInvitations = 'removeInvitations';
  static const String _pathUpdateInscriptions = 'updateInscritions';
  static const String _pathMedecinStats = 'statsMedecin';

  ConfApi({required String baseUrl}) : _baseUrl = baseUrl;

  Future<String> checkLogin(String username, String password) async {
    // Build request to check login
    final req = Uri.http(_baseUrl, _pathCheckLogin, {
      'username': username,
      'password': password,
    });
    // send request and save response
    final res = await _httpClient.get(req);
    // check response
    if (res.statusCode != 200) {
      throw ConfRequestFailure();
    }
    // decode response
    final json = jsonDecode(res.body) as Map<String, dynamic>;
    // check if found something
    if (json.isEmpty) {
      throw ConfNotFound();
    }
    var message = LoginMessage.fromJson(json);
    var result = "unknown";
    if (message.result == "SUCCESS") {
      return message.type!;
    }
    return result;
  }

  Future<List<Conference>> fetchConferences(String idMedecin) async {
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathGetConferences, {
      'idMedecin': idMedecin,
    });
    // send request and save response
    final res = await _httpClient.get(req);
    // check response
    if (res.statusCode != 200) {
      throw ConfRequestFailure();
    }
    // decode response
    final json = jsonDecode(res.body) as List<dynamic>;
    // check if found something
    return json.map((e) => Conference.fromJson(e)).toList();
  }

  Future<List<Medecin>> fetchMedecins() async {
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathListMedecin);
    // send request and save response
    final res = await _httpClient.get(req);
    // check response
    if (res.statusCode != 200) {
      throw ConfRequestFailure();
    }
    // decode response
    final json = jsonDecode(res.body) as List<dynamic>;
    // check if found something
    if (json.isEmpty) {
      throw ConfNotFound();
    }

    return json.map((e) => Medecin.fromJson(e)).toList();
  }

  Future<List<Conference>> fetchAllConferences() async {
    // Build request to fetch conference
    final req = Uri.http(_baseUrl, _pathListConf);
    // send request and save response
    final res = await _httpClient.get(req);
    // check response
    if (res.statusCode != 200) {
      throw ConfRequestFailure();
    }
    // decode response
    final json = jsonDecode(res.body) as List<dynamic>;
    // check if found something
    if (json.isEmpty) {
      throw ConfNotFound();
    }

    return json.map((e) => Conference.fromJson(e)).toList();
  }

  Future<int> insertConference(Conference conference) async {
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathAddConf);
    // send request and save response
    final res = await _httpClient.post(req,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(conference.toJson()));
    // decode response
    final json = jsonDecode(res.body);
    // check if found something
    if (json.isEmpty) {
      throw ConfNotFound();
    }
    return json['id'];
  }

  Future<void> insertInvitation(
      int pkConference, String medecin, String idCommercial) async {
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathInvConf);
    // send request and save response
    final res = await _httpClient.post(
      req,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        "conference": pkConference.toString(),
        "medecin": medecin,
        "commercial": idCommercial
      }),
    );
    // check response
    if (res.statusCode != 200) {
      throw ConfRequestFailure();
    }
    // decode response
    final json = jsonDecode(res.body);
    // check if found something
    if (json.isEmpty) {
      throw ConfNotFound();
    }
  }

  Future<void> updateValidationConference(
      int pkConference, String? technicien) async {
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathValidateConf);
    // send request and save response
    final res = await _httpClient.post(
      req,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        "conference": pkConference.toString(),
        "technicien": technicien,
      }),
    );

    final json = jsonDecode(res.body);
    // check if found something
    if (json.isEmpty) {
      throw ConfNotFound();
    }
  }

  Future<bool> addInscription(Inscription inscription) async {
    print("Adding inscripition to external db");
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathAddInscription);
    // send request and save response
    try {
      final res = await _httpClient.post(
        req,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(inscription.toJson()),
      );

      final json = jsonDecode(res.body);
      if (json["result"] == "SUCCESS") {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  Future<bool> removeInscription(Inscription inscription) async {
    print("Removing inscripition to external db");
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathRemoveInscription);
    // send request and save response
    try {
      final res = await _httpClient.post(
        req,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(inscription.toJson()),
      );

      final json = jsonDecode(res.body);
      if (json["result"] == "SUCCESS") {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  Future<bool> removeInvitation(Invitation invitation) async {
    print("Removing invitation from external db");
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathRemoveInvitation);
    // send request and save response
    try {
      final res = await _httpClient.post(
        req,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(invitation.toJson()),
      );

      final json = jsonDecode(res.body);
      if (json["result"] == "SUCCESS") {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  Future<String> removeInvitations(List<Invitation> invitations) async {
    print("Removing invitation from external db");
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathRemoveInvitations);
    // send request and save response
    try {
      final res = await _httpClient.post(
        req,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(invitations),
      );

      final json = jsonDecode(res.body);
      return json["result"];
    } catch (e) {
      return "FAILED";
    }
  }

  Future<String> updateInscriptions(List<Inscription> inscriptions) async {
    print("Updating inscriptions from external db");
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathUpdateInscriptions);
    // send request and save response
    try {
      final res = await _httpClient.post(
        req,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(inscriptions),
      );

      final json = jsonDecode(res.body);
      return json["result"];
    } catch (e) {
      return "FAILED";
    }
  }

  Future<List<StatMedecin>> fetchStatsMedecin() async {
    print("fetch stats");
    // Build request to fetch invitations
    final req = Uri.http(_baseUrl, _pathMedecinStats);
    // send request and save response
    try {
      final res = await _httpClient.get(req);
      // check response
      if (res.statusCode != 200) {
        throw ConfRequestFailure();
      }
      // decode response
      final json = jsonDecode(res.body) as List<dynamic>;
      // check if found something
      print("size " + json.length.toString());
      return json.map((e) => StatMedecin.fromJson(e)).toList();
    } catch (e) {
      print(e);
      print("object");
      return [];
    }
  }
}

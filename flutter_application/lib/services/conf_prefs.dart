import 'package:shared_preferences/shared_preferences.dart';

class ConfSharedPreferences {
  final SharedPreferences _preferences;
  final _idKey = "idUser";
  final _typeKey = "typeUser";
  final _changesKey = "changes";

  ConfSharedPreferences({required SharedPreferences plugin})
      : _preferences = plugin {
    setChanges = <String>[];
  }

  void clear() {
    _preferences.clear();
  }

  /* --- User ID Information ------------------------------------------ */

  String? get id {
    return _preferences.getString(_idKey);
  }

  set setId(String value) {
    _preferences.setString(_idKey, value);
  }

  /* --- User Type Information ------------------------------------------ */

  String? get type {
    return _preferences.getString(_typeKey);
  }

  set setType(String value) {
    _preferences.setString(_typeKey, value);
  }

  /* --- Saved information to send to server ----------------------------- */

  List<String>? get changes {
    return _preferences.getStringList(_changesKey);
  }

  set setChanges(List<String> value) {
    _preferences.setStringList(_changesKey, value);
  }

  void addChange(String change) {
    var tmp = changes!;
    tmp.add(change);
    setChanges = tmp;
  }
}

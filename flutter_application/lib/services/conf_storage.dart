import 'package:flutter_application/model/conference.dart';
import 'package:flutter_application/model/inscription.dart';
import 'package:flutter_application/model/medecin.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ConfStorage {
  final String tableMedecin = 'Medecins';
  final String tableConference = 'Conferences';
  final String dbName = 'pi.db';
  final String createConferences =
      '''CREATE TABLE Conferences(PK_Conference INTEGER PRIMARY KEY, 
          Medecin TEXT NULL, CommercialIns TEXT NULL, CommercialInv TEXT NULL,
          Titre TEXT NOT NULL, Lieu TEXT NULL, DateDebut DATETIME NOT 
          NULL, DateFin DATETIME NOT NULL, DateInscription TEXT NULL, 
          TechnicienValide TEXT NULL, TechnicienResp TEXT NULL, CommercialResp 
          TEXT NULL, Written INTEGER NOT NULL DEFAULT 0, NbrInvites INTEGER NULL,
          NbrInscrits INTEGER NULL)''';
  final String createMedecins =
      '''CREATE TABLE Medecins(Email TEXT PRIMARY KEY, Nom TEXT NOT NULL, 
          Prenom TEXT NOT NULL, Password TEXT NOT NULL)''';

  static ConfStorage? _instance;
  ConfStorage._();

  Database? db;

  factory ConfStorage() {
    _instance ??= ConfStorage._();
    return _instance!;
  }

  bool get isInitialized => db != null;

  /// Initialize the local database and creates the table Conferences and
  /// Medecins.
  Future<void> initDb() async {
    db = await openDatabase(
        join(
          await getDatabasesPath(),
          dbName,
        ), onCreate: (db, version) async {
      await db.execute(createConferences);
      await db.execute(createMedecins);
    }, version: 2);
  }

  /// Recuperate all the conferences from the database
  Future<List<Conference>> getConferences() async {
    print("getting conference");
    final data = await db!.query(tableConference);
    return data.map((e) {
      print(e);
      return Conference.fromJson2(e);
    }).toList();
  }

  /// Recuperate all the medecins from the database
  Future<List<Medecin>> getMedecins() async {
    print("getting medecins");
    final data = await db!.query(tableMedecin);
    return data.map((e) => Medecin.fromJson(e)).toList();
  }

  /// Inserts a conference [conf] into the database
  /// Returns the id of the added entry if it was successfully inserted
  /// else returns 0
  Future<int> insertConference(Conference conf) async {
    return await db!.insert(tableConference, conf.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  /// Inserts a medecin [medecin] into the database.
  ///
  /// Returns the id of the added entry if it was successfully inserted
  /// else returns 0
  Future<int> insertMedecin(Medecin medecin) async {
    return await db!.insert(tableMedecin, medecin.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<int> setDateInscription(Inscription inscription) {
    var query = 'UPDATE ' + tableConference;
    query += ' SET DateInscription = ? WHERE PK_Conference = ?';
    var date = inscription.dateInscription;
    var params = [
      (date != null) ? date.toIso8601String() : null,
      inscription.conference,
    ];
    return db!.rawUpdate(query, params);
  }

  Future<int> setFlag(int pk, int flag) async {
    var query = "UPDATE " + tableConference;
    query += ' SET Written = ? WHERE PK_Conference = ?';
    return db!.rawUpdate(query, [flag, pk]);
  }

  Future<int> removeInvitation(int pk) {
    var query = "DELETE FROM " + tableConference;
    query += " WHERE PK_Conference = ?";
    return db!.rawDelete(query, [pk]);
  }

  Future<List<Inscription>> getZeroFlags() async {
    var query = "SELECT * FROM " + tableConference;
    query += " WHERE Written = 0";
    final data = await db!.rawQuery(query);
    return data.map((e) => Inscription.fromJson(e)).toList();
  }

  Future<void> flushTables() async {
    await db!.rawDelete('DELETE FROM ' + tableConference);
    await db!.rawDelete('DELETE FROM ' + tableMedecin);
  }
}

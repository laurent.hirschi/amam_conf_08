import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_application/model/conference.dart';
import 'package:flutter_application/model/inscription.dart';
import 'package:flutter_application/model/invitation.dart';
import 'package:flutter_application/model/medecin.dart';
import 'package:flutter_application/model/stat_medecin.dart';
import 'package:flutter_application/services/conf_api.dart';
import 'package:flutter_application/services/conf_prefs.dart';
import 'package:flutter_application/services/conf_storage.dart';

enum UserRoles { unknown, technicien, commercial, medecin }

class ConfRepository {
  final ConfStorage storage;
  final ConfApi api;
  final ConfSharedPreferences _sharedPreferences;

  ConfRepository({
    required this.storage,
    required this.api,
    required ConfSharedPreferences preferences,
  }) : _sharedPreferences = preferences;

  Future<void> insertAPIConference(
      Conference conference, bool invitation) async {
    int pkConference = await api.insertConference(conference);
    if (invitation) {
      await api.insertInvitation(
        pkConference,
        conference.medecin!,
        _sharedPreferences.id!,
      );
    }
  }

  Future<void> inviteAPIConference(String pkConference, String medecin) async {
    await api.insertInvitation(
      int.parse(pkConference),
      medecin,
      _sharedPreferences.id!,
    );
  }

  Future<int> insertSQLConference(Conference conference) async {
    return await storage.insertConference(conference);
  }

  Future<List<Conference>> getSQLConferences() async {
    return await storage.getConferences();
  }

  Future<List<Conference>> getAPIConferences() async {
    return await api.fetchConferences(_sharedPreferences.id!);
  }

  Future<List<Conference>> getAPIAllConferences() async {
    return await api.fetchAllConferences();
  }

  Future<void> updateValidationAPIConference(
      int pkconference, bool validate) async {
    await api.updateValidationConference(
        pkconference, validate ? _sharedPreferences.id! : null);
  }

  Future<int> checkLogin(String username, String password) async {
    final type = await api.checkLogin(username, password);
    int result = 0;
    if (type == UserRoles.technicien.toString() ||
        type == UserRoles.commercial.toString() ||
        type == UserRoles.medecin.toString()) {
      // Saving the username and type in the shared preferences
      _sharedPreferences.setId = username;
      _sharedPreferences.setType = type;
      // return the index of the value in enum
      // so that the view can display the correct view
      result = UserRoles.values
          .firstWhere((element) => element.toString() == type)
          .index;
    }
    return result;
  }

  void flush() {
    storage.flushTables();
  }

  Future<List<Medecin>> getMedecins() async {
    if (kIsWeb) {
      return await api.fetchMedecins();
    }
    var local = await storage.getMedecins();
    // if empty do getInvitations
    // else load conferences
    if (local.isNotEmpty) {
      print("fetching locally");
      return local;
    } else {
      print("fetching online");
      // fetch data online
      var result = await api.fetchMedecins();
      // load conferences
      // save conferences locally
      if (result.isNotEmpty) {
        for (var i = 0; i < result.length; i++) {
          await storage.insertMedecin(result[i]);
        }
      }
      return result;
    }
  }

  /* --- Liste Inscription and Invitation functions ------------------- */
  Future<bool> acceptInvitation(Inscription inscription) async {
    var isAccepted = false;
    int result = -1;
    if (!kIsWeb) {
      // write locally the change
      result = await storage.setDateInscription(inscription);
    } else {
      result = 1;
    }

    if (result == 1) {
      // write to external db
      var success = await api.addInscription(inscription);
      if (!kIsWeb) {
        setWrittenFlag(success, inscription);
      }
      isAccepted = true;
    } else {
      print("An error occured when accepting invite locally");
    }
    return isAccepted;
  }

  Future<bool> rejectInvitation(Inscription inscription) async {
    var isRejected = false;
    int result = -1;
    if (!kIsWeb) {
      // remove locally
      result = await storage.setDateInscription(inscription);
    } else {
      result = 1;
    }
    if (result == 1) {
      // write to external db
      var success = await api.removeInscription(inscription);
      if (!kIsWeb) {
        setWrittenFlag(success, inscription);
      }
      isRejected = true;
    } else {
      print("An error occured when accepting invite locally");
    }
    return isRejected;
  }

  void setWrittenFlag(bool success, Inscription inscription) {
    if (success) {
      print("Inscription written to external db");
      storage.setFlag(inscription.conference, 1);
    } else {
      print("Inscription not written to external db");
      storage.setFlag(inscription.conference, 0);
    }
  }

  Future<bool> deleteInvitation(Invitation invitation) async {
    var isDone = false;
    int result = -1;
    if (!kIsWeb) {
      // remove locally
      result = await storage.removeInvitation(invitation.conference);
    } else {
      result = 1;
    }

    if (result == 1) {
      // write to external db
      var success = await api.removeInvitation(invitation);
      if (!kIsWeb) {
        if (success) {
          print("Invitation deleted from external db");
        } else {
          print("Invitation not deleted from external db");
          _sharedPreferences.addChange(invitation.toString());
        }
      }
      isDone = true;
    } else {
      print("An error occured when removing invite locally");
    }
    return isDone;
  }

  Future<String> retryInvitationChanges() async {
    print("Retry changes to external db");
    var resultDB = await storage.getZeroFlags();
    var resultPrefs = _sharedPreferences.changes;
    if (resultDB.isNotEmpty) {
      print("resultDb not empty");
      return await api.updateInscriptions(resultDB);
    }
    if (resultPrefs != null) {
      if (resultPrefs.isNotEmpty) {
        print("resultPrefs not empty");
        return await api.removeInvitations(
            resultPrefs.map((e) => jsonEncode(e) as Invitation).toList());
      }
    }
    return "SUCCESS";
  }

  ConfSharedPreferences get shared {
    return _sharedPreferences;
  }

  Future<List<StatMedecin>> getAPIStatMedecin() async {
    return api.fetchStatsMedecin();
  }
}

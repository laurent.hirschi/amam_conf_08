class Invitation {
  String medecin;
  int conference;

  Invitation({
    required this.medecin,
    required this.conference,
  });

  Map<String, dynamic> toJson() => {
        'Medecin': medecin,
        'Conference': conference,
      };
}

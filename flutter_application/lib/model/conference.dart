import 'dart:convert';

import 'package:flutter_application/model/inscription.dart';
import 'package:flutter_application/model/invitation.dart';
import 'package:flutter_application/model/lieu.dart';

class Conference {
  int pkConference;
  String? medecin;
  String? commercialInv;

  String? commercialIns;
  String titre;
  String? lieu;
  DateTime dateDebut;
  DateTime dateFin;
  DateTime? dateInscription;
  String? techValidate;
  String? commercialResp;
  String? technicienResp;
  int written;
  int? nbrInvites;
  int? nbrInscrits;

  Conference({
    required this.pkConference,
    this.medecin,
    this.commercialIns,
    this.commercialInv,
    required this.titre,
    this.lieu,
    required this.dateDebut,
    required this.dateFin,
    this.dateInscription,
    required this.written,
    this.nbrInvites,
    this.nbrInscrits,
  });

  // Constructor for incoming JSON
  Conference.fromJson(Map<String, dynamic> json)
      : pkConference = json['PK_Conference'],
        medecin = json['Medecin'],
        commercialIns = json['CommercialIns'],
        commercialInv = json['CommercialInv'],
        titre = json['Titre'],
        lieu = Lieu.fromJson(json['Lieu']).lieu,
        dateDebut = DateTime.parse(json['DateDebut']),
        dateFin = DateTime.parse(json['DateFin']),
        dateInscription = (json['DateInscription'] != null)
            ? DateTime.parse(json['DateInscription'])
            : null,
        techValidate = json['TechnicienValide'],
        technicienResp = json["TechnicienResp"],
        commercialResp = json["CommercialResp"],
        written = json["Written"] ?? 1,
        nbrInvites = json['NbrInvites'],
        nbrInscrits = json['NbrInscrits'];

  // Constructor for incoming JSON
  Conference.fromJson2(Map<String, dynamic> json)
      : pkConference = json['PK_Conference'],
        medecin = json['Medecin'],
        commercialIns = json['CommercialIns'],
        commercialInv = json['CommercialInv'],
        titre = json['Titre'],
        lieu = json['Lieu'],
        dateDebut = DateTime.parse(json['DateDebut']),
        dateFin = DateTime.parse(json['DateFin']),
        dateInscription = (json['DateInscription'] != null)
            ? DateTime.parse(json['DateInscription'])
            : null,
        techValidate = json['TechnicienValide'],
        technicienResp = json["TechnicienResp"],
        commercialResp = json["CommercialResp"],
        written = json["Written"] ?? 1,
        nbrInvites = json['NbrInvites'],
        nbrInscrits = json['NbrInscrits'];

  // Convert Conference to JSON
  Map<String, dynamic> toJson() => {
        'PK_Conference': pkConference,
        'Medecin': medecin,
        'CommercialIns': commercialIns,
        'CommercialInv': commercialInv,
        'Titre': titre,
        'Lieu': lieu,
        'DateDebut': dateDebut.toIso8601String(),
        'DateFin': dateFin.toIso8601String(),
        'DateInscription': (dateInscription != null)
            ? dateInscription!.toIso8601String()
            : null,
        "TechnicienValide": techValidate,
        'TechnicienResp': technicienResp,
        'CommercialResp': commercialResp,
        'Written': written,
        "NbrInvites": nbrInvites,
        "NbrInscrits": nbrInscrits,
      };

  bool isValidate() {
    return techValidate != null;
  }

  String getStats() {
    return nbrInscrits != null && nbrInvites != null
        ? nbrInvites.toString() + "/" + nbrInscrits.toString()
        : "";
  }

  Inscription convert2Inscription() {
    return Inscription(
      medecin: medecin!,
      conference: pkConference,
      dateInscription: dateInscription,
      commercial: commercialIns,
    );
  }

  Invitation convert2Invitation() {
    return Invitation(
      medecin: medecin!,
      conference: pkConference,
    );
  }
}

class Inscription {
  String medecin;
  int conference;
  DateTime? dateInscription;
  String? commercial;

  Inscription({
    required this.medecin,
    required this.conference,
    required this.dateInscription,
    this.commercial,
  });

  Map<String, dynamic> toJson() => {
        'Medecin': medecin,
        'Conference': conference,
        'CommercialIns': commercial,
        'DateInscription': (dateInscription != null)
            ? dateInscription!.toIso8601String()
            : null,
      };

  Inscription.fromJson(Map<String, dynamic> json)
      : conference = json['PK_Conference'],
        medecin = json['Medecin'],
        commercial = json['CommercialIns'],
        dateInscription = (json['DateInscription'] != null)
            ? DateTime.parse(json['DateInscription'])
            : null;
}

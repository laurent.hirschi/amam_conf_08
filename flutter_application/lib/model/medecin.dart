class Medecin {
  String email;
  String nom;
  String prenom;
  String password;

  Medecin(
      {required this.email,
      required this.nom,
      required this.prenom,
      required this.password});

  Medecin.fromJson(Map<String, dynamic> json)
      : email = json['Email'],
        nom = json['Nom'],
        prenom = json['Prenom'],
        password = json['Password'];

  Map<String, dynamic> toJson() => {
        'Email': email,
        'Nom': nom,
        'Prenom': prenom,
        'Password': password,
      };
}

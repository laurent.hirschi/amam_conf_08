class Lieu {
  String lieu;

  Lieu({required this.lieu});

  Lieu.fromJson(Map<String, dynamic> json) : lieu = json["Lieu"];

  Map<String, dynamic> toJson() => {
        'Lieu': lieu,
      };
}

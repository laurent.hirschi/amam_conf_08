class StatMedecin {
  String medecin;
  int nbInscriptions;
  int nbInvitations;

  StatMedecin({
    required this.medecin,
    required this.nbInscriptions,
    required this.nbInvitations,
  });

  StatMedecin.fromJson(Map<String, dynamic> json)
      : medecin = json['Medecin'],
        nbInscriptions = json['NbInscriptions'],
        nbInvitations = json['NbInvitations'];
}

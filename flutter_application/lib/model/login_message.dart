class LoginMessage {
  String result;
  String? type;

  LoginMessage({required this.result, this.type});

  // Constructor for incoming JSON
  LoginMessage.fromJson(Map<String, dynamic> json)
      : result = json['result'],
        type = json['type'];

  // Convert Conference to JSON
  Map<String, dynamic> toJson() => {
        'result': result,
        'type': type,
      };
}

class Conferences {
  List<dynamic> invitations;
  List<dynamic> inscriptions;

  Conferences({required this.inscriptions, required this.invitations});

  Conferences.fromJson(Map<String, dynamic> json)
      : invitations = json['invitations'],
        inscriptions = json['inscriptions'];

  // Convert Conference to JSON
  Map<String, dynamic> toJson() => {
        'invitations': invitations,
        'inscriptions': inscriptions,
      };
}

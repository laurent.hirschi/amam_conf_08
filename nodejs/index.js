const express = require("express");
var bodyParser = require("body-parser");
const app = express();
const port = 3000;
const test = require("./key.json");
const mariadb = require("mariadb");
const JSONBig = require("json-bigint");

const pool = mariadb.createPool({
  host: test.host,
  port: 3306,
  user: test.user,
  password: test.password,
  connectionLimit: test.connectionLimit,
  database: "DBConferences",
});

app.use(bodyParser.json());

// GET
app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/conference", async (req, res) => {

  let connection;
  try {
    connection = await pool.getConnection(); 
    //console.log(Date.now()+" REQUEST RECEIVE FROM "+  requestIp.getClientIp(req))
    console.log("connected");
    var sqlNbrInscrits = "Select PK_Conference,Medecin,Commercial as CommercialIns,Titre,Lieu,DateDebut,DateFin,TechnicienValide,Inscription.DateInscription, count(Inscription.Conference) as NbrInscrits, null  as NbrInvites  from Conference LEFT OUTER JOIN Inscription ON Conference.PK_Conference= Inscription.Conference GROUP BY Conference.PK_Conference "
    var sqlNbrInvites = "Select count(Invitation.Conference) as NbrInvites, Commercial from Conference LEFT OUTER JOIN Invitation ON Conference.PK_Conference= Invitation.Conference GROUP BY Conference.PK_Conference;"
    var rowsNbrInscrits = await connection.query(sqlNbrInscrits);
    var rowsNbrInvites = await connection.query(sqlNbrInvites);

    for (var i = 0; i < rowsNbrInscrits.length; i++) {
      rowsNbrInscrits[i]["NbrInvites"] = rowsNbrInvites[i]["NbrInvites"];
      rowsNbrInscrits[i]["CommercialInv"] = rowsNbrInvites[i]["Commercial"];
    }
    result = JSONBig.parse(JSONBig.stringify(rowsNbrInscrits));

    res.send(result);
    console.log(result);
  } catch (error) {
    console.log(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
});
/*
CREATE VIEW invitation_inscription AS
SELECT Invitation.Medecin, Invitation.Conference, Invitation.Commercial As `CommercialInv`, 
i1.Commercial AS `CommercialIns`, i1.DateInscription
FROM Invitation
LEFT JOIN
	Inscription i1
ON  i1.Medecin=Invitation.Medecin AND i1.Conference=Invitation.Conference;
*/
app.get('/getConferencesById', async (req, res) => {
  let connection;
  try {
      connection = await pool.getConnection();
      let id = req.query.idMedecin;
      if (id) {
        var sqlConferences = "SELECT Conference.*, i.CommercialInv, i.CommercialIns, i.DateInscription, i.Medecin "+
        "FROM Conference JOIN invitation_inscription i ON Conference.PK_Conference=i.Conference WHERE i.Medecin=?";
        var rows = await connection.query(sqlConferences,id);
        delete rows['meta'];
        console.log(rows);
        res.send(rows);
      }
  } catch (error) {
    console.log(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.get("/login", async (req, res) => {
  console.log("login");
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction
      // Create conference
      let email = req.query.username;
      let password = req.query.password;
      var resultTechnicien = await connection.query("SELECT * FROM Technicien WHERE email=?", [
        email,
      ]);
      delete resultTechnicien['meta'];
      var result;
      var type;
      if (resultTechnicien.length != 1) {
        var resultMedecin = await connection.query("SELECT * FROM Medecin WHERE email=?", [
          email,
        ]);
        delete resultMedecin['meta'];
        if (resultMedecin.length != 1) {
          var resultCommercial = await connection.query("SELECT * FROM Commercial WHERE email=?", [
            email,
          ]);
          delete resultCommercial['meta'];
          if (resultCommercial.length == 1) {
            result = resultCommercial[0];
            type = "UserRoles.commercial";
          }
        } else {
          result = resultMedecin[0];
          type = "UserRoles.medecin";
        }
      } else {
        result = resultTechnicien[0];
        type = "UserRoles.technicien";
      }
      var message;
      if (result) {
        if (result.Password === password) {
          message = {"result":"SUCCESS", "type":type}
        } else {
          message = {"result":"ERROR"}
        }
      } else {
        message = {"result":"ERROR"}
      }
      res.send(message);   
    } catch (err) {
      console.error("Error on request, reverting changes: ", err);
    }finally {
      if (connection) {
        connection.release();
      }
    }
});

app.get("/medecin", async (req, res) => {
  let connection;
  try {
    connection = await pool.getConnection();
    console.log("connected");
    rows = await connection.query("SELECT * from Medecin");
    res.send(rows);
    console.log(rows);
  } catch (error) {
    console.log(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
});


// POST addInvitation
app.post("/addInvitation", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    // Create invitation
    let conference = req.body.conference;
    let medecin = req.body.medecin;
    let commercial = req.body.commercial;

    var result = await connection.query(
      "INSERT INTO Invitation (Conference, Medecin, Commercial) VALUES (?,?,?)",
      [conference, medecin, commercial]
    );

    await connection.commit(); // Commit Changes
    resultAddConf = JSONBig.parse(JSONBig.stringify(result)).insertId;
    res.send({"result":"SUCCESS", "id":resultAddConf});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.post("/addInscription", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    let inscription = req.body;
    let medecin = inscription.Medecin;
    let conference = inscription.Conference;
    let commercial = inscription.CommercialIns;
    let dateInscription = inscription.DateInscription;

    await connection.query(
      "INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES (?,?,?,?)",
      [medecin, conference, dateInscription, commercial]
    );
    await connection.commit(); // Commit Changes
    res.send({"result":"SUCCESS"});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

//POST
app.post("/addconference", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    // Create conference
    let conference = req.body;
    console.log(conference);
    const result = await pool.query(
      "INSERT INTO `Conference` (`TechnicienValide`,`TechnicienResp`, `CommercialResp`, `Titre`, `Lieu`, `DateDebut`, `DateFin`, `NbMaxMedecin`) VALUES (?,?,?, ? ,?, ?, ? , 5)",
      [
        null,
        conference.TechnicienResp,
        conference.CommercialResp, // For the moment null
        conference.Titre,
        {"Lieu": conference.Lieu},
        conference.DateDebut,
        conference.DateFin,
      ]
    );
    
    resultAddConf = JSONBig.parse(JSONBig.stringify(result)).insertId;

    await connection.commit(); // Commit Changes
    res.send({"result":"SUCCESS", "id":resultAddConf});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.post("/validateconference", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    let conference = req.body.conference;
    let technicien = req.body.technicien;

    var result = await connection.query(
      "UPDATE Conference SET TechnicienValide = ? WHERE PK_Conference = ?",
      [technicien, conference]
    );

    await connection.commit(); // Commit Changes
    res.send({"result":"SUCCESS"});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.post("/removeInscription", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    let inscription = req.body;
    let medecin = inscription.Medecin;
    let conference = inscription.Conference;

    await connection.query(
      "DELETE FROM Inscription WHERE `Inscription`.`Medecin` = ? AND `Inscription`.`Conference` = ?",
      [medecin, conference]
    );
    await connection.commit(); // Commit Changes
    res.send({"result":"SUCCESS"});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.post("/removeInvitation", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    let inscription = req.body;
    let medecin = inscription.Medecin;
    let conference = inscription.Conference;

    await connection.query(
      "DELETE FROM Invitation WHERE `Invitation`.`Medecin` = ? AND `Invitation`.`Conference` = ?",
      [medecin, conference]
    );
    await connection.commit(); // Commit Changes
    res.send({"result":"SUCCESS"});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.post("/removeInvitations", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    let invitations = req.body;
    for (let i = 0; i < invitations.length; i++) {
      var result = await connection.query(
        "DELETE FROM Invitation WHERE `Invitation`.`Medecin` = ? AND `Invitation`.`Conference` = ?",
        [invitations[i].medecin, invitations[i].conference]
      );
      console.log(result);
    }

    
    await connection.commit(); // Commit Changes
    res.send({"result":"SUCESS","conflicts":"[]"});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.post("/updateInscritions", async (req, res) => {
  var bad = -1;
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    let inscriptions = req.body;
    for (let i = 0; i < inscriptions.length; i++) {
      bad = i;
      if (inscriptions[i].DateInscription == null) {
        var result = await connection.query(
          "DELETE FROM Inscription WHERE `Inscription`.`Medecin` = ? AND `Inscription`.`Conference` = ?",
          [inscriptions[i].Medecin, inscriptions[i].Conference]
        );
        console.log(result);
      } else {
        var result = await connection.query(
          "INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES (?,?,?,?)",
          [inscriptions[i].Medecin, inscriptions[i].Conference, inscriptions[i].DateInscription, inscriptions[i].CommercialIns]
        );
        console.log(result);
      }
      
    }
    await connection.commit(); // Commit Changes
    res.send({"result":"SUCESS","conflicts":"[]"});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.send({"result":"FAILED","conflicts":"["+bad+"]"});
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.get("/statsMedecin", async (req, res) => {

  let connection;
  try {
    connection = await pool.getConnection(); 
    console.log("connected");
    var sqlNbInscriptions = "SELECT Medecin, COUNT(*) AS NbInscriptions FROM Inscription GROUP BY Medecin"
    var rowsNbInscritpions = await connection.query(sqlNbInscriptions);
    var sqlNbInvitations = "SELECT Medecin, COUNT(*) AS NbInvitations FROM Invitation GROUP BY Medecin"
    var rowsNbInvitations = await connection.query(sqlNbInvitations);

    for (var i = 0; i < rowsNbInscritpions.length; i++) {
      rowsNbInscritpions[i]["NbInvitations"] = rowsNbInvitations[i]["NbInvitations"];
    }
    result = JSONBig.parse(JSONBig.stringify(rowsNbInscritpions));

    res.send(result);
    console.log(result);
  } catch (error) {
    console.log(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

module.exports = {
  getConnection: function () {
    return new Promise(function (resolve, reject) {
      pool
        .getConnection()
        .then(function (connection) {
          resolve(connection);
        })
        .catch(function (error) {
          reject(error);
        });
    });
  },
};

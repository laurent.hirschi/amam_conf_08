USE DBConferences;


-- Insert into Commercial 5 rows
INSERT INTO Commercial (Email, Nom, Prenom, Password) VALUES ('com1@com1.ch', 'Com1', 'Com1', 'com1');
INSERT INTO Commercial (Email, Nom, Prenom, Password) VALUES ('com2@com2.ch', 'Com2', 'Com2', 'com2');
INSERT INTO Commercial (Email, Nom, Prenom, Password) VALUES ('com3@com3.ch', 'Com3', 'Com3', 'com3');
INSERT INTO Commercial (Email, Nom, Prenom, Password) VALUES ('com4@com4.ch', 'Com4', 'Com4', 'com4');
INSERT INTO Commercial (Email, Nom, Prenom, Password) VALUES ('com5@com5.ch', 'Com5', 'Com5', 'com5');

-- Insert into Medecin 6 rows
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med1@med1.ch', 'Med1', 'Med1', 'med1');
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med2@med2.ch', 'Med2', 'Med2', 'med2');
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med3@med3.ch', 'Med3', 'Med3', 'med3');
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med4@med4.ch', 'Med4', 'Med4', 'med4');
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med5@med5.ch', 'Med5', 'Med5', 'med5');
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med6@med6.ch', 'Med6', 'Med6', 'med6');

-- Insert into Technicien 5 rows
INSERT INTO Technicien (Email, Nom, Prenom, Password) VALUES ('tec1@tec1.ch', 'Tec1', 'Tec1', 'tec1');
INSERT INTO Technicien (Email, Nom, Prenom, Password) VALUES ('tec2@tec2.ch', 'Tec2', 'Tec2', 'tec2');
INSERT INTO Technicien (Email, Nom, Prenom, Password) VALUES ('tec3@tec3.ch', 'Tec3', 'Tec3', 'tec3');
INSERT INTO Technicien (Email, Nom, Prenom, Password) VALUES ('tec4@tec4.ch', 'Tec4', 'Tec4', 'tec4');
INSERT INTO Technicien (Email, Nom, Prenom, Password) VALUES ('tec5@tec5.ch', 'Tec5', 'Tec5', 'tec5');

-- Insert into Conference 3 rows
INSERT INTO Conference (Titre, Lieu, DateDebut, DateFin, NbMaxMedecin, TechnicienValide, TechnicienResp, CommercialResp)
VALUES ('Conf1', '{"Lieu": "Lieu1"}', '2022-10-10', '2022-10-20', 5, 'tec1@tec1.ch', 'tec1@tec1.ch', NULL);
INSERT INTO Conference (Titre, Lieu, DateDebut, DateFin, NbMaxMedecin, TechnicienValide, TechnicienResp, CommercialResp)
VALUES ('Conf2', '{"Lieu": "Lieu2"}', '2022-10-10', '2022-10-20', 5, 'tec2@tec2.ch', NULL, 'com2@com2.ch');
INSERT INTO Conference (Titre, Lieu, DateDebut, DateFin, NbMaxMedecin, TechnicienValide, TechnicienResp, CommercialResp)
VALUES ('Conf3', '{"Lieu": "Lieu3"}', '2022-10-10', '2022-10-20', 5, 'tec3@tec3.ch', 'tec3@tec3.ch', NULL);

-- Insert into Invitation 7 rows
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med1@med1.ch', 1, 'com1@com1.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med2@med2.ch', 2, 'com2@com2.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med1@med1.ch', 3, 'com1@com1.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med2@med2.ch', 3, 'com1@com1.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med3@med3.ch', 3, 'com1@com1.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med4@med4.ch', 3, 'com1@com1.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med5@med5.ch', 3, 'com1@com1.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med6@med6.ch', 3, 'com1@com1.ch');


-- Insert into Inscirption 7 rows
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med1@med1.ch', 1, '2022-04-07', 'com1@com1.ch');
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med2@med2.ch', 2, '2022-04-07', 'com2@com2.ch');
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med1@med1.ch', 3, '2022-04-07', 'com3@com3.ch');
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med2@med2.ch', 3, '2022-04-07', 'com3@com3.ch');
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med3@med3.ch', 3, '2022-04-07', 'com3@com3.ch');
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med4@med4.ch', 3, '2022-04-07', 'com3@com3.ch');
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med5@med5.ch', 3, '2022-05-09', 'com3@com3.ch');
